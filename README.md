#Author
Newton Blair
Contanct email: nblair@uoregon.edu

#Description of software
The software creates a server that takes a GET request on a local host server 127.0.0.1:5000 for a file name. The request gets processed and the Server reponds by either sending the file if it exists on the server, 
or it gets served an error header and correlating error html page based on the error caught. If the name of the file contains .. or ~ the server sends the 403 error header and html page. If the file is just not found on
the server the server sends the 404 header and html page.

#Purpose of project 
A "getting started" manual for Dockers. CIS 322, Introduction to Software Engineering, at the University of Oregon. Reference: https://docs.docker.com/engine/reference/builder/

#Building and Running my Dockerfile

* Go to the web folder in the repository. Read every line of the docker file and the simple flask app.

* Build the simple flask app image using

  ```
  docker build -t newton-proj .
  ```
  
* Run the container using
  
  ```
  docker run -d -p 5000:5000 newton-proj
  ```

* Launch and search the server for file names using web browser url http://127.0.0.1:5000/file_name
