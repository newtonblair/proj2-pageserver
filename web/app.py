from flask import Flask, abort, render_template

app = Flask(__name__)

@app.route("/<name>")
def search(name):
    try:
        return render_template(name), 200
    except:
	    if "//" in name or ".." in name or "~" in name:
		    abort(403)
	    else:
		    abort(404)

@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404
	
@app.errorhandler(403)
def forbidden(error):
    return render_template('403.html'), 403
	
if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
